$(function(){
    var interimaire     = $('#contrat_nomPrenom');
    var resultSearch    = $('#result-search');
    var input           = $('#contrat_interimaire');
    var btnSave         = $('#contrat_save');
    var url             = interimaire.data('url');
    var error           = interimaire.data('error');
    var spinner         = interimaire.data('spinner');

    //INITIAL
    if(input.val()){
        btnSave.prop('disabled',false);
    }


    /**
     * Recherche des intérimaires
     */
    interimaire.keyup(function(e){
        var interimaireTerm = $(interimaire).val();

        if(interimaireTerm.length > 0) {
            $.ajax({
                url: url,
                type: 'POST',
                data: {term: interimaireTerm},
                beforeSend: function () {
                    resultSearch.html(spinner);
                },
                success: function (data) {
                    resultSearch.html(data);
                    input.val('');
                    btnSave.prop('disabled', true);
                },
                error: function () {
                    resultSearch.html(error);
                }
            });
            e.stopImmediatePropagation();
            return false;
        }else{
            btnSave.prop('disabled', true);
            resultSearch.html('');
        }
    });



    /**
     * Au clic sur un interimaire :
     * remplir l'input hidden
     * remplir l'identité de la personne dans le champ déclencheur de l'ajax
     **/
    $(document).on('click','.interimaire-item',function(){
        var idInterimaire   = $(this).data('id');
        var txtInterimaire  = $(this).text();

        input.val(idInterimaire);
        interimaire.val(txtInterimaire);
        resultSearch.html('');
        btnSave.prop('disabled',false);
    });
});