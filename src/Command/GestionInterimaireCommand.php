<?php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
//Service
use App\Service\ClotureContrat;
use App\Service\CompleterVille;
use App\Service\EmailNotification;


class GestionInterimaireCommand extends Command
{
    // bin/console app:gestion-interimaire
    protected static $defaultName = 'app:gestion-interimaire';
    private $clotureContrat;
    private $completeVille;
    private $emailNotofication;



    public function __construct(ClotureContrat $clotureContrat, CompleterVille $completeVille, EmailNotification $emailNotification)
    {
        $this->clotureContrat    = $clotureContrat;
        $this->completeVille     = $completeVille;
        $this->emailNotofication = $emailNotification;

        parent::__construct();
    }



    protected function configure()
    {
        $this   ->setDescription('Traitement des données des intérimaires')
                ->setHelp('Cette commande permet
                        - d\'envoyer une notification à l\'intérimaire 1 jour avant le début de son contrat
                        - Rempli le champ "ville" s\'il manque en table
                        - Cloture les contrats terminés
                    ');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cloturerContrat = $this->clotureContrat->cloture();
        $completerVille  = $this->completeVille->completer();
        $notifier        = $this->emailNotofication->notifier(1);

        $action1 = $output->section();
        $action2 = $output->section();
        $action3 = $output->section();

        $action1->writeln('Nombre de contrats changés en statut "terminé" : ' . $cloturerContrat);
        $action2->writeln('Nombre de villes manquantes mises à jour : ' .$completerVille);
        $action3->writeln('Nombre de contrats notifiés par email : ' .$notifier);
    }
}