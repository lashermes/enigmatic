<?php
namespace App\Service;

use App\Entity\Contrat;

class Stat
{
    const PATH_FILE = 'stat.csv';
    const PATH_FOLDER = 'stat';


    public function __construct()
    {
        $this->checkFolder();
    }


    /**
     * Export CSV des occurences de statuts des contrats dans une intervalle de Date
     * séparateur : ,
     * enclosure : "
     *
     * @param $datas array      le nombre d'occurence de chaque statut
     * @param $debut, $fin \DateTime
     */
    public function csv(array $datas,\DateTime $debut, \DateTime$fin)
    {
        $de         = $debut->format('d-m-Y');
        $a          = $fin->format('d-m-Y');
        $path_file  = $this::PATH_FOLDER . '/' . $de . '_' . $a . '_' . $this::PATH_FILE;

        $fp         = fopen($path_file, 'w');
        foreach ($datas as $fields) {
            fputcsv($fp, $fields);
        }
        fclose($fp);

        return $path_file;
    }


    /*
     * Crée le dossier stat/ s'il n'existe pas
     */
    public function checkFolder(){
        if(!file_exists($this::PATH_FOLDER)){
            mkdir($this::PATH_FOLDER);
        }
    }


    /*
     * Récupère le tableau du repository
     * [
     * 0=> [
     *      'nb'=>3,
     *      'nom_statut'=>false
     *  ]
     * ]
     */
    public function checkDatas(array $datas){
        $datasOk = [];
        foreach($datas as $data){
            if(is_null($data['nom_statut'])){
                $datasOk[] = ['Terminé', $data['nb']];
            }elseif(!$data['nom_statut']){
                $datasOk[] = ['En attente', $data['nb']];
            }else{
                $datasOk[] = ['En cours', $data['nb']];
            }
        }

        return $datasOk;
    }
}