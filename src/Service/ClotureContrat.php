<?php
namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
//Entity
use App\Entity\Contrat;


class ClotureContrat
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    /*
     * Cloture les contrats terminés
     */
    public function cloture()
    {
        $nbContrats = $this->em->getRepository(Contrat::class)->updateObsolete();

        return $nbContrats;
    }
}