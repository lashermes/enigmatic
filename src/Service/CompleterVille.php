<?php
namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
//Entity
use App\Entity\Interimaire;
//Service
use App\Service\Api\GeoApiGouv;

/*
 * Completer les villes manquantes des intérimaires par le cp*
 */
class CompleterVille
{
    private $geoApiGouv;
    private $em;


    public function __construct(GeoApiGouv $geoApiGouv, EntityManagerInterface $em)
    {
        $this->geoApiGouv   = $geoApiGouv;
        $this->em = $em;
    }


    /*
     * Trouver les interimaires dont les villes sont manquantes
     * Set + update
     */
    public function completer()
    {
        $interimaireVillesManquantes = $this->em->getRepository(Interimaire::class)->findBy(['ville'=>null]);
        $interimaireVilleOk          = $this->setVille($interimaireVillesManquantes);
        $this->em->flush();

        return count($interimaireVilleOk);
    }


    /*
     * @param Interimaire[]     intérimaires dont la ville est null
     */
    public function setVille(array $interimaireVillesManquantes)
    {
        $interimaireVilleOk = [];
        foreach($interimaireVillesManquantes as $interimaire){
            $ville = $this->geoApiGouv->getVilleByCp($interimaire->getCp());
            $interimaire->setVille($ville);
            $interimaireVilleOk[] = $interimaire;

        }

        return $interimaireVilleOk;
    }
}