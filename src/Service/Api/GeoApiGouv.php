<?php
namespace App\Service\Api;

/*
 * Récupérer les datas de l'API geo.api.gouv
 * On pourrait passer directement par l'Api pour récupérer les données
 * Je crée une classe tampon, qui sera le seul "endroit" à modifier
 * si le site geo.api.gouv venait à modifier la manière d'accéder aux datas
 */
class GeoApiGouv
{
    const URL = 'https://geo.api.gouv.fr/';


    /*
     * On ne conserve que la première ville du résultat si le CP renvoie à plusieurs villes
     */
    public function getVilleByCp($cp){
        $url    = $this::URL . 'communes?codePostal=' . $cp;

        $villes = json_decode(file_get_contents($url));
        $ville  = $villes[0]->nom;

        return $ville;
    }
}