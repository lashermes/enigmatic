<?php
namespace App\Service;

use App\Entity\Contrat;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Environment;

class EmailNotification
{
    private $em;
    private $mailer;
    private $twig;


    public function __construct(EntityManagerInterface $em, \Swift_Mailer $mailer, Environment $twig)
    {
        $this->em     = $em;
        $this->mailer = $mailer;
        $this->twig   = $twig;
    }


    /*
     * @param int $nbJourBefore     Nombre de jours avant le début de son contrat
     * @param \Swift_Mailer
     */
    public function notifier(int $nbJourBefore)
    {
        $contrats = $this->em->getRepository(Contrat::class)->findContratBeforeXDay($nbJourBefore);

        foreach ($contrats as $contrat){
            $destinataire = $contrat->getInterimaire()->getEmail();

            $message = (new \Swift_Message('Notification '.$nbJourBefore.' jour(s) avant contrat'))
                ->setFrom('nlashermes@gmail.com')
                ->setTo($destinataire)
                ->setBody($this->twig->render( '_inc/email/notification.html.twig', ['nb'=>$nbJourBefore] ),
                    'text/html'
                );

            $this->mailer->send($message);
        }

        return count($contrats);
    }
}