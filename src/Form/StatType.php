<?php 
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
//Type
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;



class StatType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('debut',DateType::class,[
                'required'=>true,
                'label'=>'Du',
                'years'=>range(date('Y'),date('Y')-30)
            ])
            ->add('fin',DateType::class,[
                'required'=>true,
                'label'=>'Au',
                'years'=>range(date('Y'),date('Y')-30)
            ])
            ->add('save',SubmitType::class,[
                'label'=>'Envoyer',
            ])
        ;
    }
}