<?php 
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
//Entity
use App\Entity\Contrat;
//Type
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ContratType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('interimaire',HiddenType::class,[
                'required'=>true,
                'mapped'=> false,
            ])
            ->add('nomPrenom',TextType::class,[
                'mapped'=>false,
                'required'=>true,
                'label'=>'Intérimaire',
            ])
            ->add('debut',DateType::class,[
                'required'=>true,
                'label'=>'Début du contrat',
                'years'=>range(date('Y'),date('Y')+10)
            ])
            ->add('fin',DateType::class,[
                'required'=>true,
                'label'=>'Fin du contrat',
                'years'=>range(date('Y'),date('Y')+10)
            ])
            ->add('statut',ChoiceType::class,[
                'required'=>true,
                'label'=>'Statut',
                'choices'  => [
                    'En attente' => false,
                    'En cours' => true,
                    'Terminé' => null,
                ],
            ])
            ->add('save',SubmitType::class,[
                'label'=>'Sauvegarder',
                'attr'=>['disabled'=>'disabled']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contrat::class,
        ]);
    }
}