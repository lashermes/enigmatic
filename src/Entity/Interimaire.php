<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InterimaireRepository")
 */
class Interimaire
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomPrenom;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $cp;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Contrat", mappedBy="interimaire", orphanRemoval=true)
     */
    private $contrats;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SuiviMission", mappedBy="interimaire", orphanRemoval=true)
     */
    private $suiviMissions;

    public function __construct()
    {
        $this->contrats = new ArrayCollection();
        $this->suiviMissions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomPrenom(): ?string
    {
        return $this->nomPrenom;
    }

    public function setNomPrenom(string $nomPrenom): self
    {
        $this->nomPrenom = $nomPrenom;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCp(): ?string
    {
        return $this->cp;
    }

    public function setCp(?string $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * @return Collection|Contrat[]
     */
    public function getContrats(): Collection
    {
        return $this->contrats;
    }

    public function addContrat(Contrat $contrat): self
    {
        if (!$this->contrats->contains($contrat)) {
            $this->contrats[] = $contrat;
            $contrat->setInterimaire($this);
        }

        return $this;
    }

    public function removeContrat(Contrat $contrat): self
    {
        if ($this->contrats->contains($contrat)) {
            $this->contrats->removeElement($contrat);
            // set the owning side to null (unless already changed)
            if ($contrat->getInterimaire() === $this) {
                $contrat->setInterimaire(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SuiviMission[]
     */
    public function getSuiviMissions(): Collection
    {
        return $this->suiviMissions;
    }

    public function addSuiviMission(SuiviMission $suiviMission): self
    {
        if (!$this->suiviMissions->contains($suiviMission)) {
            $this->suiviMissions[] = $suiviMission;
            $suiviMission->setInterimaire($this);
        }

        return $this;
    }

    public function removeSuiviMission(SuiviMission $suiviMission): self
    {
        if ($this->suiviMissions->contains($suiviMission)) {
            $this->suiviMissions->removeElement($suiviMission);
            // set the owning side to null (unless already changed)
            if ($suiviMission->getInterimaire() === $this) {
                $suiviMission->setInterimaire(null);
            }
        }

        return $this;
    }
}
