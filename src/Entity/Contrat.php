<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContratRepository")
 */
class Contrat
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank(message="ce champ est obligatoire")
     * @Assert\Expression(  "value < this.getFin()",
     *                      message="La date de début ne peut être inférieure ou égale à celle de fin")
     */
    private $debut;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank(message="ce champ est obligatoire")
     */
    private $fin;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Interimaire", inversedBy="contrats")
     * @ORM\JoinColumn(nullable=false)
     */
    private $interimaire;

    /**
     * 0 => en attente, 1 => en cours, null => terminé
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $statut;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SuiviMission", mappedBy="contrat", orphanRemoval=true)
     */
    private $suiviMissions;

    public function __construct()
    {
        $this->suiviMissions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDebut(): ?\DateTimeInterface
    {
        return $this->debut;
    }

    public function setDebut(\DateTimeInterface $debut): self
    {
        $this->debut = $debut;

        return $this;
    }

    public function getFin(): ?\DateTimeInterface
    {
        return $this->fin;
    }

    public function setFin(\DateTimeInterface $fin): self
    {
        $this->fin = $fin;

        return $this;
    }

    public function getInterimaire(): ?Interimaire
    {
        return $this->interimaire;
    }

    public function setInterimaire(?Interimaire $interimaire): self
    {
        $this->interimaire = $interimaire;

        return $this;
    }

    public function getStatut(): ?bool
    {
        return $this->statut;
    }

    public function setStatut(?bool $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * @return Collection|SuiviMission[]
     */
    public function getSuiviMissions(): Collection
    {
        return $this->suiviMissions;
    }

    public function addSuiviMission(SuiviMission $suiviMission): self
    {
        if (!$this->suiviMissions->contains($suiviMission)) {
            $this->suiviMissions[] = $suiviMission;
            $suiviMission->setContrat($this);
        }

        return $this;
    }

    public function removeSuiviMission(SuiviMission $suiviMission): self
    {
        if ($this->suiviMissions->contains($suiviMission)) {
            $this->suiviMissions->removeElement($suiviMission);
            // set the owning side to null (unless already changed)
            if ($suiviMission->getContrat() === $this) {
                $suiviMission->setContrat(null);
            }
        }

        return $this;
    }
}
