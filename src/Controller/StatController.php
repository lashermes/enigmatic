<?php

namespace App\Controller;

use App\Entity\Contrat;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
//Form
use App\Form\StatType;
//Service
use App\Service\Stat;


class StatController extends AbstractController
{
    /**
     * Récupérer le nombre de statuts en cours, terminés, commencés via un formulaire
     * qui détermine l'intervale pour établir la stat exportée dans un .csv
     *
     * @Route("/stats", name="stat")
     *
     * @param $request Request
     * @param $stat Stat
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, Stat $stat)
    {
        $form = $this->createForm(StatType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $datas  = $form->getData();
            $debut  = $datas['debut'];
            $fin    = $datas['fin'];

            $contrats = $this->getDoctrine()->getRepository(Contrat::class)->stat($debut,$fin);
            $datasOk = $stat->checkDatas($contrats);
            $path = $stat->csv($datasOk,$debut,$fin);

            return $this->file($path);
        }

        return $this->render('stat/index.html.twig.', ['form' => $form->createView() ]);
    }
}
