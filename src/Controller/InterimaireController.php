<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
//Entity
use App\Entity\Interimaire;


class InterimaireController extends AbstractController
{
    /**
     * @Route("/interimaire-search", name="interimaire_search")
     *
     * @param $request Request
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function search(Request $request)
    {
        $term = $request->request->get('term');
        $interimaires = $this->getDoctrine()->getRepository(Interimaire::class)->search($term);

        return $this->render('interimaire/search.html.twig',['interimaires'=>$interimaires]);
    }
}
