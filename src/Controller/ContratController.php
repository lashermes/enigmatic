<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
//Entity
use App\Entity\Contrat;
use App\Entity\Interimaire;
//Type
use App\Form\ContratType;

class ContratController extends AbstractController
{
    /**
     * Liste des contrats
     *
     * @Route("/contrats", name="contrats")
     *
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function contrats()
    {
        $contrats = $this->getDoctrine()->getRepository(Contrat::class)->findAll();

        return $this->render('contrat/liste.html.twig', [ 'contrats'=>$contrats ]);
    }



    /**
     * Création d'un contrat :
     * L'intérimaire est trouvé via ajax qui retourne l'id de l'interimaire
     * Après soumission du formulaire l'intérimaire est find puis set dans le contrat
     *
     * @Route("/contrat-add", name="contrat_add")
     *
     * @param $request Request
     * @return Symfony\Component\HttpFoundation\Response    Vue de la page
     * @return RedirectResponse
     */
    public function add(Request $request)
    {
        $contrat        = new Contrat();
        $form           = $this->createForm(ContratType::class, $contrat);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $idInterimaire  = intval($request->get('contrat')['interimaire']);
            $interimaire = $em->getRepository(Interimaire::class)->find($idInterimaire);
            $contrat->setInterimaire($interimaire);

            $em->persist($contrat);
            $em->flush();
            $this->addFlash( 'ok', 'Le contrat a été créé');

            return $this->redirectToRoute('contrats');
        }


        return $this->render('contrat/add-edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * Edition d'un contrat
     *
     * @Route("/contrat-edit/{id}", name="contrat_edit")
     *
     * @param int $id Contrat
     * @param $request Request
     *
     * @return Symfony\Component\HttpFoundation\Response    Vue de la page
     * @return RedirectResponse                             au post du formulaire
     */
    public function edit(int $id = null, Request $request)
    {
        $contrat = $this->getDoctrine()->getRepository(Contrat::class)->find($id);
        if(!$contrat){
            $this->addFlash( 'error', 'Le contrat à modifier n\'existe pas');

            return $this->redirectToRoute('contrats');
        }

        $form = $this->createForm(ContratType::class, $contrat);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $idInterimaire  = intval($request->get('contrat')['interimaire']);
            $interimaire = $em->getRepository(Interimaire::class)->find($idInterimaire);
            $contrat->setInterimaire($interimaire);

            $em->persist($contrat);
            $em->flush();
            $this->addFlash( 'ok', 'Le contrat a été modifié');

            return $this->redirectToRoute('contrats');
        }


        return $this->render('contrat/add-edit.html.twig', [
            'form' => $form->createView(),
            'contrat'=>$contrat
        ]);
    }



    /**
     * Supprimer un contrat
     *
     * @Route("/contrat/remove/{id}", name="contrat_delete")
     *
     * @param int $id       Contrat
     * @param Request
     * @return RedirectResponse
     */
    public function delete(int $id = null, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $contrat = $this->getDoctrine()->getRepository(Contrat::class)->find($id);

        if(!$contrat){
            $this->addFlash( 'error', 'L\'intérimaire à supprimer n\'existe pas');
            return $this->redirectToRoute('contrats');
        }

        $em->remove($contrat);
        $em->flush();

        return $this->redirectToRoute('contrats');
    }
}
