<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
//Entity
use App\Entity\Interimaire;
use App\Entity\Contrat;
use App\Entity\SuiviMission;

class AppFixtures extends Fixture
{
    /*
     * Créer des intérimaires pour le test (puisque CRUD non obligatoire)
     * ville laissée null
     *
     *
     **/
    public function load(ObjectManager $manager)
    {
        $this->interimaires($manager);
    }




    //Création intérimaires (CRUD non demandé)
    public function interimaires($manager){
        $interimaires = [
            ['NomPrenom'=>'Lashermes Nicolas','Email'=>'lashermes.nicolas@live.fr','Cp'=>'38500'],
            ['NomPrenom'=>'Dupuis Charles','Email'=>'dupuis.charles@live.fr','Cp'=>'42153'],
            ['NomPrenom'=>'Arridu Sophie','Email'=>'arridu.sophie@live.fr','Cp'=>'72000']
        ];

        //Interimaires
        foreach ($interimaires as $interimaire) {
            $newInterimaire = new Interimaire();

            foreach ($interimaire as $field => $value){
                $method = 'set'.$field;
                $newInterimaire->$method($value);
            }

            $manager->persist($newInterimaire);
            $manager->flush();

            $this->contrats($newInterimaire,$manager);
        }
    }



    //Création contrat
    public function contrats($newInterimaire, ObjectManager $manager){

        $contrats = [
            ['38500'=>(new Contrat())->setDebut(new \DateTime('2014-01-'.rand(0,28)))->setFin(new \DateTime('2014-02-'.rand(0,28)))->setStatut(null)],
            ['38500'=>(new Contrat())->setDebut(new \DateTime('2014-01-'.rand(0,28)))->setFin(new \DateTime('2014-02-'.rand(0,28)))->setStatut(null)],
            ['38500'=>(new Contrat())->setDebut(new \DateTime('2014-01-'.rand(0,28)))->setFin(new \DateTime('2014-02-'.rand(0,28)))->setStatut(0)],

            ['42153'=>(new Contrat())->setDebut(new \DateTime('2014-02-'.rand(0,28)))->setFin(new \DateTime('2014-03-'.rand(0,28)))->setStatut(1)],
            ['42153'=>(new Contrat())->setDebut(new \DateTime('2014-02-'.rand(0,28)))->setFin(new \DateTime('2014-03-'.rand(0,28)))->setStatut(1)],
            ['42153'=>(new Contrat())->setDebut(new \DateTime('2014-02-'.rand(0,28)))->setFin(new \DateTime('2014-03-'.rand(0,28)))->setStatut(1)],
            ['42153'=>(new Contrat())->setDebut(new \DateTime('2014-02-'.rand(0,28)))->setFin(new \DateTime('2014-03-'.rand(0,28)))->setStatut(0)],

            ['72000'=>(new Contrat())->setDebut(new \DateTime('2014-03-'.rand(0,28)))->setFin(new \DateTime('2014-04-'.rand(0,28)))->setStatut(1)],
            ['72000'=>(new Contrat())->setDebut(new \DateTime('2014-03-'.rand(0,28)))->setFin(new \DateTime('2014-04-'.rand(0,28)))->setStatut(1)],
            ['72000'=>(new Contrat())->setDebut(new \DateTime('2014-03-'.rand(0,28)))->setFin(new \DateTime('2014-04-'.rand(0,28)))->setStatut(1)],
            ['72000'=>(new Contrat())->setDebut(new \DateTime('2014-03-'.rand(0,28)))->setFin(new \DateTime('2014-04-'.rand(0,28)))->setStatut(0)]
        ];


        foreach($contrats as $rowCcontrat){
            foreach($rowCcontrat as $key => $contrat){
                if($key == $newInterimaire->getCp()){
                    $contrat->setInterimaire($newInterimaire);

                    $manager->persist($contrat);
                    $manager->flush();

                    $this->suivis($contrat, $newInterimaire, $manager);
                }
            }
        }
    }


    /*
     * Créations des suivis de missions (car CRUD non demandé)
     */
    public function suivis($contrat, $newInterimaire, $manager){
        $suivi = new SuiviMission();
        $suivi  ->setInterimaire($newInterimaire)
                ->setContrat($contrat)
                ->setNote(rand(0,10))
                ->setStatut(0)
            ;
        $manager->persist($suivi);
        $manager->flush();
    }
}
