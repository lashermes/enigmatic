<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190216103305 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE contrat (id INT AUTO_INCREMENT NOT NULL, interimaire_id INT NOT NULL, debut DATETIME NOT NULL, fin DATETIME NOT NULL, statut TINYINT(1) DEFAULT NULL, INDEX IDX_603499935CA839FE (interimaire_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE interimaire (id INT AUTO_INCREMENT NOT NULL, nom_prenom VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, cp VARCHAR(15) NOT NULL, ville VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE suivi_mission (id INT AUTO_INCREMENT NOT NULL, interimaire_id INT NOT NULL, contrat_id INT NOT NULL, note SMALLINT NOT NULL, statut TINYINT(1) NOT NULL, INDEX IDX_15A006995CA839FE (interimaire_id), INDEX IDX_15A006991823061F (contrat_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contrat ADD CONSTRAINT FK_603499935CA839FE FOREIGN KEY (interimaire_id) REFERENCES interimaire (id)');
        $this->addSql('ALTER TABLE suivi_mission ADD CONSTRAINT FK_15A006995CA839FE FOREIGN KEY (interimaire_id) REFERENCES interimaire (id)');
        $this->addSql('ALTER TABLE suivi_mission ADD CONSTRAINT FK_15A006991823061F FOREIGN KEY (contrat_id) REFERENCES contrat (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE suivi_mission DROP FOREIGN KEY FK_15A006991823061F');
        $this->addSql('ALTER TABLE contrat DROP FOREIGN KEY FK_603499935CA839FE');
        $this->addSql('ALTER TABLE suivi_mission DROP FOREIGN KEY FK_15A006995CA839FE');
        $this->addSql('DROP TABLE contrat');
        $this->addSql('DROP TABLE interimaire');
        $this->addSql('DROP TABLE suivi_mission');
    }
}
