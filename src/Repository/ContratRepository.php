<?php

namespace App\Repository;

use App\Entity\Contrat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Contrat|null find($id, $lockMode = null, $lockVersion = null)
 * @method Contrat|null findOneBy(array $criteria, array $orderBy = null)
 * @method Contrat[]    findAll()
 * @method Contrat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContratRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Contrat::class);
    }

     /**
      * Retourne des contrats dont au moins le début ou la fin sont compris dans l'intervale
      * choisie par l'internaute
      *
      * @param $debut \DateTime     Date de début de contrat choisi par l'internaute
      * @param $fin \DateTime       Date de fin de contrat choisi par l'internaute
      * @return Contrat[]           Retourne un array de Contrat [0]=>statut:null, [1]=>statut:0, [2]=>statut:1
      */
    public function stat($debut,$fin)
    {
        $qb = $this->createQueryBuilder('c');
        $contrats = $qb ->select('COUNT(c) as nb, c.statut as nom_statut')
                        ->andWhere('c.fin BETWEEN :debut AND :fin')
                        ->orWhere('c.debut BETWEEN :debut AND :fin')
                        ->setParameter(':debut', $debut)
                        ->setParameter(':fin', $fin)
                        ->groupBy('c.statut')
                        ->orderBy('c.statut')
                        ->getQuery()
                        ->getResult()
                    ;

        return $contrats;
    }


    /*
     * Cloture les contrats terminés : statut: null
     */
    public function updateObsolete(){
        $today = (new \DateTime('midnight'));

        $qb = $this->createQueryBuilder('c');
        $nbContrats = $qb ->update()
                        ->set('c.statut','null')
                        ->andWhere('c.fin < :now')
                        ->setParameter('now', $today)
                        ->getQuery()
                        ->getResult()
                        ;

        return $nbContrats;
    }


    /*
     * Trouver les contrats qui se terminent avant X jour avant le début de son contrat
     */
    public function findContratBeforeXDay(int $nb){
        $oneDayBeforeNow = (new \DateTime('midnight'))->modify('-' . $nb . ' day');
        $today = (new \DateTime('midnight'));

        $qb = $this->createQueryBuilder('c');
        $contrats = $qb ->andWhere('c.debut BETWEEN :oneDayBeforeNow AND :today')
                        ->setParameters(['oneDayBeforeNow'=>$oneDayBeforeNow,
                                         'today'=>$today
                                        ])
                        ->getQuery()
                        ->getResult()
                    ;

        return $contrats;
    }
}
