<?php

namespace App\Repository;

use App\Entity\Interimaire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Interimaire|null find($id, $lockMode = null, $lockVersion = null)
 * @method Interimaire|null findOneBy(array $criteria, array $orderBy = null)
 * @method Interimaire[]    findAll()
 * @method Interimaire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InterimaireRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Interimaire::class);
    }

    /**
     * Recherche les interimaires avec les meilleures notes en premier,
     * les meilleurs se trient par :
     * - la meilleure note moyenne : de toutes les notes actives ou non
     *
     * @return Interimaire[]    Retourne un tableau d'object de type Interimaires
     */
    public function search(string $term){
        $qb = $this->createQueryBuilder('i');
        $interimaires = $qb ->addSelect('AVG(s.note) as noteMoyenne')
                            ->leftJoin('i.suiviMissions','s')
                            ->andWhere('i.nomPrenom LIKE :term')
                            ->setParameter('term', '%'.$term.'%')
                            ->orderBy('noteMoyenne','DESC')
                            ->addGroupBy('i')
                            ->getQuery()
                            ->getResult()
                            ;

        return $interimaires;
    }
}
