<?php

namespace App\Repository;

use App\Entity\SuiviMission;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SuiviMission|null find($id, $lockMode = null, $lockVersion = null)
 * @method SuiviMission|null findOneBy(array $criteria, array $orderBy = null)
 * @method SuiviMission[]    findAll()
 * @method SuiviMission[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SuiviMissionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SuiviMission::class);
    }

    // /**
    //  * @return SuiviMission[] Returns an array of SuiviMission objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SuiviMission
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
